package com.saharat.rentalcar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

data class CarAdapter(private val carList: ArrayList<Car>)
    :RecyclerView.Adapter<CarAdapter.CarViewsHolder>(){

    var onItemClick : ((Car) -> Unit)? = null

    class CarViewsHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val imageView : ImageView = itemView.findViewById(R.id.imageView)
        val textView : TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.car_item, parent, false)
        return CarViewsHolder(view)
    }

    override fun onBindViewHolder(holder: CarViewsHolder, position: Int) {
        var car = carList[position]
        holder.imageView.setImageResource(car.image)
        holder.textView.text = car.name

        holder.imageView.setOnClickListener {
            onItemClick?.invoke(car)
        }
    }

    override fun getItemCount(): Int {
        return carList.size
    }
}
