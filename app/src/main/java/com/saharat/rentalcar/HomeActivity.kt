package com.saharat.rentalcar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class HomeActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var carList: ArrayList<Car>
    private lateinit var carAdapter: CarAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)

        carList= ArrayList()

        carList.add(Car(R.drawable.bmw_m5_img,  "BMW M5 G-power"))
        carList.add(Car(R.drawable.ford_mustang, "Ford Mustang GT"))
        carList.add(Car(R.drawable.audi, "Audi A7 2022"))
        carList.add(Car(R.drawable.mercedes,  "Mercedes-Benz"))

        carAdapter = CarAdapter(carList)
        recyclerView.adapter = carAdapter

        carAdapter.onItemClick = {
            val intent = Intent(this, DetailedActivity::class.java)
            intent.putExtra("car", it)
            startActivity(intent)
        }
    }

}